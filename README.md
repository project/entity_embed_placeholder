# Entity Embed Placeholder Module

[Entity Embed](https://www.drupal.org/project/entity_embed_placeholder) module
allows any entity embedded to have a placeholder in the form of a custom
template on CKEditor.

## Requirements

* Drupal 9 or higher.
* [Embed](https://www.drupal.org/project/embed) module.
* [Entity Embed](https://www.drupal.org/project/entity_embed) module.

## Installation

Entity Embed Placeholder can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## Configuration

* Install and enable [Embed](https://www.drupal.org/project/embed) module.
* Install and enable [Entity Embed](https://www.drupal.org/project/entity_embed)
  module.
* If you need to customize the embed preview, there are several options:
  - Create a custom admin theme and override the template and styles from there
  by copy / pasting the templates into your custom theme.
  - If you are using a custom front end theme, add a custom stylesheet to
  CKEditor using the `ckeditor5-stylesheets` attribute to your
  [theme .info file](https://www.drupal.org/docs/core-modules-and-themes/core-modules/ckeditor-5-module/how-to-style-custom-content-in-ckeditor-5)
  - Override the template from a custom module using
  [hook_theme_registry_alter](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21theme.api.php/function/hook_theme_registry_alter/8.2.x), for instance:
  ```
  /**
  * Implements hook_theme_registry_alter().
  */
  function test_module_theme_registry_alter(&$theme_registry) { dsm('works');
    $module_path = \Drupal::service('extension.list.module')->getPath('test_module');
    $theme_registry['node__embed_preview']['path'] = $module_path . '/templates';
    $theme_registry['node__embed_preview']['template'] = 'node-preview';
  }
  ```
## Troubleshooting

If the changes you are doing aren't reflected, take in account that you'll need
to clear the cache and in most cases embed a fresh entity for the changes
to take effect.
